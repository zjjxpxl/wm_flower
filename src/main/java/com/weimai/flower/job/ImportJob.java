package com.weimai.flower.job;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import com.alibaba.fastjson.JSONObject;

import com.weimai.flower.app.entity.FlowerNameEntity;
import com.weimai.flower.app.service.FlowerNameService;
import com.weimai.flower.util.PinyinUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ImportJob {

    private final static Logger log = LoggerFactory.getLogger(ImportJob.class);

    @Autowired
    private FlowerNameService flowerNameService;

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public static LinkedBlockingQueue<FlowerNameEntity> queue = new LinkedBlockingQueue<>();

    private static ExecutorService executor =
            new ThreadPoolExecutor(1, 1024, 60L, TimeUnit.MILLISECONDS,
                                                                               new LinkedBlockingDeque<>(1024), Executors
                                                                                       .defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());
    @Scheduled(fixedDelay = 1000)
    public void importCheck() {

        if(queue.isEmpty()){
            return;
        }

        executor.execute(()->{

            while (true){
                FlowerNameEntity entity = queue.poll();
                if(entity==null){
                    return;
                }else{
                    FlowerNameEntity exist = flowerNameService.getByName(entity.getFlowerName());
                    if(exist!=null){
                        flowerNameService.setUsed(exist.getId());
                    }else{
                        flowerNameService.save(entity);
                    }
                }
            }//end foreach

        });

    }


    /*@PostConstruct
    public void test() throws IOException {

        String pathname = "D:\\drug.txt";
        File filename = new File(pathname);
        InputStreamReader reader = new InputStreamReader(
                new FileInputStream(filename));
        BufferedReader br = new BufferedReader(reader);

        int count = 0;

        //while (count<Integer.MAX_VALUE){
        while (count<1000){

            String line = br.readLine();

            if(line==null){
                break;
            }

            String spell = PinyinUtil.getPinYin(line);

            FlowerNameEntity entity = new FlowerNameEntity(line,spell,false,new Date(),new Date());

            try{
                jdbcTemplate.update("insert into flower(flower,spell,flag,create_time,update_time) "
                                    + "values(?,?,?,?,?) ",entity.getFlowerName(),entity.getSpell(),entity.convertFlag(),entity.getCreateTime(),entity.getUpdateTime());
            }catch (Exception e){
                log.warn(" save error !   messsage:" + e.getMessage() + " data:" + JSONObject.toJSONString(entity));
            }

            count++;

        }

    }*/

}
