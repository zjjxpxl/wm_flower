package com.weimai.flower.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class PageDataVO<T> {

    /**
     * 总页数
     */
    @ApiModelProperty("总页数")
    private Integer totalPage;

    /**
     * 总条数
     */
    @ApiModelProperty("总条数")
    private Long totalCount;

    /**
     * 数据
     */
    @ApiModelProperty("数据")
    private List<T> items;

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public PageDataVO() {
    }

    public PageDataVO(Integer totalPage, Long totalCount, List<T> items) {

        this.totalPage = totalPage;
        this.totalCount = totalCount;
        this.items = items;
    }



}
