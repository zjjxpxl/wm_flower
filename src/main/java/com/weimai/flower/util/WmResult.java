package com.weimai.flower.util;

import java.io.Serializable;

/**
 *
 *web统一返回结果封装
 * Copyright (c) 2017 Choice, Inc.
 * All Rights Reserved.
 * Choice Proprietary and Confidential.
 *
 * @author qianlishui
 * @since 2017年9月11日
 */
public class WmResult<T> implements Serializable {
	/**  */
	private static final long serialVersionUID = -440772889286277256L;
	private static int SUCCESS_CODEID = 0;//正常
	private static int INTERNAL_CODEID = -1;//内部错误
	private int code;
	private String message;
	private T data = (T)new Object();

	public WmResult() {
		
	}
	
	private WmResult(int code, T data, String message) {
		this.code = code;
		this.data = data;
		this.message = message;
	}

	private WmResult(int code, String message) {
		this.code = code;
		this.message = message;
	}

	private WmResult(int code, T data) {
		this.code = code;
		this.data = data;
	}
	
	public static <T> WmResult<T> success(T data) {
		return new WmResult(SUCCESS_CODEID, data, "");
	}

	public static <T> WmResult<T> success(int code,T data,String message) {
		return new WmResult(code, data, message);
	}

	public static WmResult error(int code,String message) {
		return new WmResult(code, message);
	}

	public static WmResult internalError() {
		return new WmResult(INTERNAL_CODEID, "小脉开小差去了，请您稍后重试");
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
