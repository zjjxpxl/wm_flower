package com.weimai.flower.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.web.multipart.MultipartFile;

/**
 * Copyright (c) 2017 Choice, Inc.
 * All Rights Reserved.
 * Choice Proprietary and Confidential.
 *
 * 类注释
 *
 * @author choice
 * @since 2019-07-26 20:11
 */
public class AppUtil {

    public static void copyFileUsingFileStreams(MultipartFile file, File dest)
            throws IOException {
        OutputStream output = null;
        InputStream input = null;
        try {
            input = file.getInputStream();
            output = new FileOutputStream(dest);
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = input.read(buf)) > 0) {
                output.write(buf, 0, bytesRead);
            }
        } finally {
            input.close();
            output.close();
        }
    }

}
