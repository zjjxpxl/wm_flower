package com.weimai.flower.app.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Copyright (c) 2017 Choice, Inc.
 * All Rights Reserved.
 * Choice Proprietary and Confidential.
 *
 * 花名实体类
 *
 * @author yunhao
 * @since 2019-07-26 15:56
 */
public class FlowerNameEntity implements Serializable {

    public FlowerNameEntity(){
        super();
    }

    public FlowerNameEntity(String flowerName, String spell, boolean useFlag, Date createTime, Date updateTime) {
        this.flowerName = flowerName;
        this.spell = spell;
        this.useFlag = useFlag;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    private int id;

    private String flowerName;

    private String spell;

    private boolean useFlag;

    private Date createTime;

    private Date updateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFlowerName() {
        return flowerName;
    }

    public void setFlowerName(String flowerName) {
        this.flowerName = flowerName;
    }

    public String getSpell() {
        return spell;
    }

    public void setSpell(String spell) {
        this.spell = spell;
    }

    public boolean isUseFlag() {
        return useFlag;
    }

    public void setUseFlag(boolean useFlag) {
        this.useFlag = useFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int convertFlag(){
        if(useFlag){
            return 1;
        }else{
            return 0;
        }
    }

}
