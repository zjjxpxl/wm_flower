package com.weimai.flower.app.dao;

import java.util.List;

import com.weimai.flower.app.entity.FlowerNameEntity;

public interface FlowerNameDao {
    void save(FlowerNameEntity entity);

    FlowerNameEntity getByName(String flowerName);

    List<FlowerNameEntity>  getBySpell(String spell);

    void delete(int id);

    void batchUpdate(List<FlowerNameEntity> entityList);

    void setUsed(int id);

    void setUnused(int id);

    boolean isExist(String flowerName, String spell);

    List<FlowerNameEntity> getRandomList(int limit);

    List<FlowerNameEntity> getList(String words, int useFlag, int pageSize, int currentPage);

    int getCount(String words, int useFlag);

    List<FlowerNameEntity> getAll(int useFlag);
}
