package com.weimai.flower.app.service;

import java.util.List;

import com.weimai.flower.app.entity.FlowerNameEntity;

/**
 * Copyright (c) 2017 Choice, Inc.
 * All Rights Reserved.
 * Choice Proprietary and Confidential.
 *
 *
 *
 * @author yunhao
 * @since 2019-07-26 16:05
 */
public interface FlowerNameService {
    void save(FlowerNameEntity entity);

    void delete(int id);

    void setUsed(int id);

    void setUnused(int id);

    FlowerNameEntity getByName(String flowerName);

    List<FlowerNameEntity>  getBySpell(String spell);

    boolean isExist(String flowerName, String spell);

    List<FlowerNameEntity> getRandomList(int limit);

    List<FlowerNameEntity> getList(String words, int useFlag, int pageSize, int currentPage);

    int getCount(String words, int useFlag);

    List<FlowerNameEntity> getAll(int useFlag);
}
