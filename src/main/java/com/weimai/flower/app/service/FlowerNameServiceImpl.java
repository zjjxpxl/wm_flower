package com.weimai.flower.app.service;

import java.util.List;

import com.weimai.flower.app.dao.FlowerNameDao;
import com.weimai.flower.app.entity.FlowerNameEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright (c) 2017 Choice, Inc.
 * All Rights Reserved.
 * Choice Proprietary and Confidential.
 *
 * 类注释
 *
 * @author choice
 * @since 2019-07-26 16:05
 */
@Service
public class FlowerNameServiceImpl implements FlowerNameService {

    @Autowired
    private FlowerNameDao flowerNameDao;

    @Override
    public void save(FlowerNameEntity entity) {
        flowerNameDao.save(entity);
    }

    @Override
    public void delete(int id) {
        flowerNameDao.delete(id);
    }

    @Override
    public void setUsed(int id) {
        flowerNameDao.setUsed(id);
    }

    @Override
    public void setUnused(int id) {
        flowerNameDao.setUnused(id);
    }

    @Override
    public FlowerNameEntity getByName(String flowerName) {
        return flowerNameDao.getByName(flowerName);
    }

    @Override
    public List<FlowerNameEntity>  getBySpell(String spell) {
        return flowerNameDao.getBySpell(spell);
    }

    @Override
    public boolean isExist(String flowerName, String spell) {
        return flowerNameDao.isExist(flowerName, spell);
    }

    @Override
    public List<FlowerNameEntity> getRandomList(int limit) {
        return flowerNameDao.getRandomList(limit);
    }

    @Override
    public List<FlowerNameEntity> getList(String words, int useFlag, int pageSize, int currentPage) {
        return flowerNameDao.getList(words, useFlag, pageSize, currentPage);
    }

    @Override
    public int getCount(String words, int useFlag) {
        return flowerNameDao.getCount(words, useFlag);
    }

    @Override
    public List<FlowerNameEntity> getAll(int useFlag) {
        return flowerNameDao.getAll(useFlag);
    }

}
